* Run all tests with the tap of a button!
* New Dashboard design and UI
* Rewritten NDT to increase reliability and measurements accuracy
* Middlebox tests grouped in the Performance card
* Telegram, Ndt, Dash test written in golang