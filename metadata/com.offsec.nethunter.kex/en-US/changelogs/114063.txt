v4.0.4
- Fix for scrolling in Single Handed mode
v4.0.3
- Updated sshlib
v4.0.2
- Fixes for widget names
- Fixes for Fit-to-screen scaling
- Default Input Mode support
v4.0.1
- Text contrast fix for certain devices
- Targeting new APIs
- Fix for Menu + immersive mode live-lock on some devices
v4.0.0
- Input mode bugfixes
- Security and protocol library updates
v3.9.9
- Fix for IMEs that use META_SHIFT_ON
- Fix for right-click in TouchPad mode
- ExtendedDesktopSize support for TigerVNC
v3.9.7
- 
